package day2;

import java.util.ArrayList;

public class Lab6_sajee {
	public static void main(String[] args) {
		/*
		//for loop
		for (int counter = 1; counter <=100; counter = counter+20) {
			System.out.println("Counter :" + counter);
		}
	

		//for each ใข้กับ array, collection
		int [] pageNumber = {1,2,3,4,5,6,7,8,9};
		int [] testScore = {20,40,100};
		String [] friends = {"airry", "nanny", "music"};
		
		//วิธีเขียนแบบที่ 1
		for(int i = 0 ; i < pageNumber.length; i++) {
			System.out.println(pageNumber[i]);
		}
		
// ต้องแก้ข้อมูลเมื่อมีการแก้ไขตัวแปร
//		System.out.println(pageNumber[0]);
//		System.out.println(pageNumber[1]);
//		System.out.println(pageNumber[2]);
//		System.out.println(pageNumber[3]);
//		System.out.println(pageNumber[4]);
//		System.out.println(pageNumber[5]);
//	}
		
		//วิธีเขียนแบบที่ 2 สำหรับ String
		for(String member : friends) {
			System.out.println(member);
		}
		
		//วิธีเขียนแบบที่ 2 สำหรับ int 
		for(int member : testScore) {
			System.out.println(member);
		}
		
		//while loop
		int counter = 0; 
		while (counter < 5) {
			System.out.println("Count" + counter);		
			counter++;
			
				}
	}
}
*/
	//1 
	int counter = 1;
	int sum = 0;
	int a[] = {1,2,3,4,5};

	while(counter <= 10) {
		System.out.println(counter);
		counter++;
	}
	//2 โปรแกรมหาผลรวมของเลข 1-10
	// 1 2 3 4 5 6 7 8 9 10 
	//ต้องบวกที่ละคู่แล้วจำค่าเอาไว้เพื่อบวกต่อ
	//ส้รางตัวแปรสำหรับเก็บค่าผลรวมพักไว้
	
		
	while(counter <= 10) {
		sum = sum + counter;
				System.out.println("Counter"+counter);
		System.out.println("Sum"+sum);
		counter++;
		
	}
		System.out.println("Total " + sum);
		
	//3 วร้างโปรแกรมหาค่าระหว่าง 1-100 ที่หาร 12 ลงตัว
	//เอา 12 ไปหารเหลือเศษ = 0 [12,24,36,48,60,72,84]
	//% = หารเอาเศษ ดังนั้นมันคือค่าที่ % 12 = 0
	//1) วน loop ตั้งแต่ 1 จนถึง 100
	//2) ถ้า ค่าตอนนั้น % 12 = 0 ให้ print ค่า
	
		while(counter <= 100) {
			if(counter%12 ==0) {
				System.out.println(counter);
			}
			counter++;
			
		}
	
		for (int x :a) {
			System.out.println(x);
		}
			 
//	ArrayList<Integer> resultList = new ArrayList<Integer>();
//	while(counter3 <= 100) {
//		if(counter3 % 12 == 0) {
//			resultList.add(counter3);
//			System.out.println("ตัวเลขที่หารด้วย 12 ลงตัว คือ " + counter3);
//		}
//		counter3 ++;
//		
//		}
//		for(Integer res : resultList) {
//			System.out.println("2) ตัวเลขที่หารด้วย 12 ลงตัว " + res);
//		}
	}
	
	
	

	
}
